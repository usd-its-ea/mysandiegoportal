package edu.sandiego.mysandiegoportal.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * SHRLGPA model.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Shrlgpa {
    /**
     * GPA.
     */
    private String gpa;
    /**
     * GPA Hours.
     */
    private String gpaHours;
    /**
     * GPA Type Ind.
     * O = Overall Institutional (Transfer + Institutional)
     * T = Transfer Credit
     * I = Institutional Credit.
     */
    private String gpaTypeInd;
    /**
     * Hours Attempted.
     */
    private String hoursAttempted;
    /**
     * Hours Passed.
     */
    private String hoursPassed;
    /**
     * Hours Earned.
     */
    private String hoursEarned;
    /**
     * Quality Points.
     */
    private String qualityPoints;
    /**
     * Level Code.
     */
    private String levlCode;
    /**
     * Activity Date.
     */
    private String activityDate;

}
