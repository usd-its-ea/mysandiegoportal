package edu.sandiego.mysandiegoportal.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

/**
 * Created by rdhiman on Mar.03.2021.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PersonDetailedInfo {

    /* Hold on Student Account. */
    //private String studentAccountHold;
    /* Emergency Notifications Phone Alert. */
    //private String emergencyHold;
    /* Hold on Student Account. */
   //private String financeHold;
    /* Hold on Student Account. */
   private String preferredFirstName;
    /* BasicPersonInfo */
    User basicPersonInfo;
    /*Person GPA Details. */
    List<Shrlgpa> shrlgpa;
    /* Department and Major Details*/
    DepartmentAndMajor departmentAndMajor;



}
