package edu.sandiego.mysandiegoportal.domain;

import static edu.sandiego.mysandiegoportal.domain.RoleNames.*;
import static edu.sandiego.mysandiegoportal.domain.RoleNames.DEPOSITED_UG;

/**
 * Created by rdhiman on Mar.17.2021.
 */
public class GroupNames {

    /*###############################################################################
            Group Names by Roles.
      ###############################################################################*/
    /*UG Student Group */
    public static final String UG_STUDENT_GROUP = "UG_STUDENT_GROUP";
    public static final String UG_APPLICANT_STUDENT_GROUP = "UG_APPLICANT_STUDENT_GROUP";
    /* Graduate Student Group */
    public static final String GR_STUDENT_GROUP = "GR_STUDENT_GROUP";
    public static final String GR_APPLICANT_STUDENT_GROUP = "GR_APPLICANT_STUDENT_GROUP";
    public static final String GR_MBA_STUDENT_GROUP = "GR_MBA_STUDENT_GROUP";
    public static final String GR_LAW_STUDENT_GROUP = "GR_LAW_STUDENT_GROUP";
    public static final String GR_ELA_STUDENT_GROUP = "GR_ELA_STUDENT_GROUP";
    /* Employee/Faculty/Staff Group */
    public static final String EMPLOYEE_GROUP = "EMPLOYEE_GROUP";
    /* Non Applicant Group */
    public static final String NON_APPLICANT_GROUP = "NON_APPLICANT_GROUP";
    /* Admin Group */
    public static final String PORTAL_ADMIN_GROUP = "PORTAL_ADMIN_GROUP";
    /*Applicant Group (UG + GRAD) */
    public static final String APPLICANT_GROUP = "APPLICANT_GROUP";
    /* Law Active */
    public static final String LAW_ACTIVE_GROUP = "LAW_ACTIVE_GROUP";
    /* Ela Active */
    public static final String ELA_ACTIVE_GROUP = "ELA_ACTIVE_GROUP";
    /* Senior */
    public static final String SENIOR_GROUP = "SENIOR_GROUP";
    /* Dept_Chair */
    public static final String DEPT_CHAIR_GROUP = "DEPT_CHAIR_GROUP";
    /* Paralegal Active */
    public static final String PARALEGAL_ACTIVE_GROUP = "PARALEGAL_ACTIVE";
    /* Undergrad Active Group */
    public static final String UNDERGRAD_ACTIVE_GROUP = "UNDERGRAD_ACTIVE_GROUP";
    /* Grad Active Group */
    public static final String GRAD_ACTIVE_GROUP = "GRAD_ACTIVE_GROUP";
    /* Grad OR Undergrad Active Group */
    public static final String GRAD_OR_UNDERGRAD_ACTIVE_GROUP = "GRAD_OR_UNDERGRAD_ACTIVE_GROUP";
    /*All Active Groups Students*/
    public static final String ALL_ACTIVE_STUDENTS_GROUP = "ALL_ACTIVE_STUDENTS_GROUP";
    /* Anyone but Applicant Group:
     * This Group contains any Active Students + Employee + Support_Roles(if any) But Not Applicants. */
    public static final String EVERYONE_BUT_APPLICANTS_GROUP = "EVERYONE_BUT_APPLICANTS_GROUP";
    /* Grad Applicant  Group.*/
    public static final String GRAD_APPLICANT_GROUP = "GRAD_APPLICANT_GROUP";
    /* Grad Admitted  Group .*/
    public static final String GRAD_ADMITTED_GROUP = "GRAD_ADMITTED_GROUP";
    /* Grad Deposited  Group .*/
    public static final String GRAD_DEPOSITED_GROUP = "GRAD_DEPOSITED_GROUP";
    /* Undergrad Applicant  Group .*/
    public static final String UG_APPLICANT_GROUP = "UG_APPLICANT_GROUP";
    /* Undergrad Admitted  Group .*/
    public static final String UG_ADMITTED_GROUP = "UG_ADMITTED_GROUP";
    /* Undergrad Deposited  Group .*/
    public static final String UG_DEPOSITED_GROUP = "UG_DEPOSITED_GROUP";
    /* All Active Students And All Applicants  Group .*/
    public static final String ALL_ACTIVE_STUDENTS_AND_ALL_APPLICANTS_GROUP = "ALL_ACTIVE_STUDENTS_AND_ALL_APPLICANTS_GROUP";
    /*Grad Deposited and Admitted Group.*/
    public static final String GRAD_DEPOSITED_AND_ADMITTED_GROUP = "GRAD_DEPOSITED_AND_ADMITTED_GROUP";
    /*Grad Deposited and Admitted Group.*/
    public static final String UG_DEPOSITED_AND_ADMITTED_GROUP = "UG_DEPOSITED_AND_ADMITTED_GROUP";
    /*UG New Student Group.*/
    public static final String UG_NEWSTUDENT_GROUP = "UG_NEWSTUDENT_GROUP";
    /*All Admitted Group: ug, gr, law(llm/jd) */
    public static final String ALL_ADMITTED_GROUP = "ALL_ADMITTED_GROUP";

    /*###############################################################################
            Group Names by Tabs.
     ###############################################################################*/
    /*Torero Hub Tab Group.*/
    public static final String TOREROHUB_TAB_GROUP = "TOREROHUB_TAB_GROUP";
    /*Torero Life Tab Group.*/
    public static final String TORERO_LIFE_TAB_GROUP = "TORERO_LIFE_TAB_GROUP";

    public static final String FACULTY_AND_DEPT_CHAIRS_TAB_GROUP = "FACULTY_AND_DEPT_CHAIRS_TAB_GROUP";

    public static final String EMPLOYEE_TAB_GROUP = "EMPLOYEE_TAB_GROUP";

    public static final String HELP_DESK_AND_SUPPORT_TAB_GROUP = "HELP_DESK_AND_SUPPORT_TAB_GROUP";

    public static final String FST_STUDENT_TAB_GROUP = "FST_STUDENT_TAB_GROUP";

    public static final String LAW_ADMISSIONS_TAB_GROUP = "LAW_ADMISSIONS_TAB_GROUP";

    public static final String GRAD_ADMISSIONS_TAB_GROUP = "GR_ADMISSIONS_TAB_GROUP";

    public static final String UG_ADMISSIONS_TAB_GROUP = "UG_ADMISSIONS_TAB_GROUP";

    public static final String ALUMNI_TAB_GROUP = "ALUMNI_TAB_GROUP";

    public static final String APPLICATIONS_AND_TOOLS_TAB_GROUP = "APPLICATIONS_AND_TOOLS_TAB_GROUP";



     /*###############################################################################
            Group Names by Page under Tabs.
     ###############################################################################*/

    public static final String STUDENT_EMPLOYMENT_PAGE_GROUP = "STUDENT_EMPLOYMENT_PAGE_GROUP";

    public static final String USDONE_PAGE_GROUP = "USDONE_PAGE_GROUP";

    public static final String MY_SUPPORT_SERVICES_PAGE_GROUP = "MY_SUPPORT_SERVICES_PAGE_GROUP";

    public static final String SAFETY_CHECK_PAGE_GROUP = "SAFETY_CHECK_PAGE_GROUP";

    public static final String EVENTS_PAGE_GROUP = "EVENTS_PAGE_GROUP";

}
