package edu.sandiego.mysandiegoportal.domain;

import edu.sandiego.custom.utils.USDPropertyHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by rdhiman on Mar.23.2021.
 */
public class ApplicationFinals {

    /**
     * Logger.
     */
    private static Logger log = LogManager.getLogger(ApplicationFinals.class.getName());
    /**
     * Main Config File Full Path.
     */
    public static final String MYSANDIEGOPORTAL_CONFIG_HOME = System.getenv("MYSANDIEGOPORTAL_CONFIG_HOME");
    public static final String MYSANDIEGOPORTAL_CONFIG_FILE_NAME = System.getenv("MYSANDIEGOPORTAL_CONFIG_FILE_NAME");
    /*File name*/
    public static String MYSANDIEGOPORTAL_CONFIG_FILE_FULL_PATH = MYSANDIEGOPORTAL_CONFIG_HOME + MYSANDIEGOPORTAL_CONFIG_FILE_NAME;

    public static final String EMPLOYEE_FAVORITE_APPLICATIONS_PROPERTY_NAME = "employee.favorite.applications.file-location";
    public static final String STUDENT_FAVORITE_APPLICATIONS_PROPERTY_NAME = "student.favorite.applications.file-location";

}
