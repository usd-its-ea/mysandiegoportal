package edu.sandiego.mysandiegoportal.domain;

/**
 * Created by rdhiman on Apr.05.2021.
 */
public class URI {

    public static final String MY_SAN_DIEGO_PORTAL_CONTEXT_NAME = "/mysandiegoportal/";
    public static final String EMPLOYEE_URI = MY_SAN_DIEGO_PORTAL_CONTEXT_NAME + "employee/";
    public static final String STUDENT_URI = MY_SAN_DIEGO_PORTAL_CONTEXT_NAME + "student/";
    public static final String UG_URI = STUDENT_URI + "undergraduate/";
    public static final String GRAD_URI = STUDENT_URI + "grad/";
    public static final String LAW_URI = STUDENT_URI + "law/";
    public static final String FST_URI = STUDENT_URI + "fst/";
    public static final String ALUMNI_URI = STUDENT_URI + "alumni/";
    public static final String ELA_URI = STUDENT_URI + "ela/";

    public static final String TORERO_LIFE_URI = STUDENT_URI + "torerolife/";

    public static final String HELP_DESK_AND_SUPPORT_URI = MY_SAN_DIEGO_PORTAL_CONTEXT_NAME + "helpdesk/";

    public static final String APPLICATIONS_AND_TOOLS_URI = MY_SAN_DIEGO_PORTAL_CONTEXT_NAME + "helpdesk/";

    public static final String COMMON_URI = MY_SAN_DIEGO_PORTAL_CONTEXT_NAME + "common/";

    public static final String EMPLOYEE_APPLICATIONS_AND_TOOLS_URI = "/mysandiegoportal/employee/applicationsAndTools";
}
