package edu.sandiego.mysandiegoportal.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by rdhiman on Mar.22.2021.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DepartmentAndMajor {

    private String primaryMajorCode;
    private String primaryMajorDesc;
    private String primaryDeptCode;
    private String primaryDeptDesc;
}
