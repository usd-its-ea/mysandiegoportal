package edu.sandiego.mysandiegoportal.domain;

/**
 * Created by rdhiman on Mar.18.2021.
 */
public class RoleNames {

    public static final String ADMITTED = "";
    public static final String ADMITTED_GR = "ADMITTED_GR";
    public static final String ADMITTED_UG = "ADMITTED_UG";
    public static final String ALCALA = "ALCALA";
    public static final String ALL_FALL = "ALL_FALL";
    public static final String ALL_FR = "ALL_FR";
    public static final String ALL_INT = "ALL_INT";
    public static final String ALL_SPRING = "ALL_SPRING";
    public static final String ALL_SUMMER = "ALL_SUMMER";
    public static final String ALL_TR = "ALL_TR";
    public static final String ALUMNI = "ALUMNI";
    public static final String APPLICANT = "APPLICANT";
    public static final String APPLICANT_FALL = "APPLICANT_FALL";
    public static final String APPLICANT_GRAD = "APPLICANT_GRAD";
    public static final String APPLICANT_INT = "APPLICANT_INT";
    public static final String APPLICANT_SPRING = "APPLICANT_SPRING";
    public static final String APPLICANT_SUMMER = "APPLICANT_SUMMER";
    public static final String APPLICANT_UG = "APPLICANT_UG";
    public static final String COE = "COE";
    public static final String DEAN = "DEAN";
    public static final String DEPOSITED = "DEPOSITED";
    public static final String DEPOSITED_GR = "DEPOSITED_GR";
    public static final String DEPOSITED_UG = "DEPOSITED_UG";
    public static final String DEPT_CHAIR = "DEPT_CHAIR";

    public static final String EMPLOYEE = "EMPLOYEE";
    public static final String FACULTY = "FACULTY";
    public static final String FINAID = "FINAID";
    public static final String FIRSTYEARSTUDENT = "FIRSTYEARSTUDENT";

    public static final String GIGYA_REG_HOUSING = "GIGYA_REG_HOUSING";

    public static final String HOLD_STUDENTACCOUNTS = "HOLD_STUDENTACCOUNTS";
    public static final String HONOR_ACCEPT = "HONOR_ACCEPT";
    public static final String HONOR_INVITE = "HONOR_INVITE";

    public static final String LAW_ADMITTED_JD = "LAW_ADMITTED_JD";
    public static final String LAW_ADMITTED_LLM = "LAW_ADMITTED_LLM";
    public static final String LAW_ADMITTED_TRANSFER_JD = "LAW_ADMITTED_TRANSFER_JD";
    public static final String LAW_ADMITTED_VISITOR_JD = "LAW_ADMITTED_VISITOR_JD";
    public static final String LAW_DEPOSIT1_JD = "LAW_DEPOSIT1_JD";
    public static final String LAW_DEPOSIT2_JD = "LAW_DEPOSIT2_JD";
    public static final String LAW_DEPOSIT_LLM = "LAW_DEPOSIT_LLM";

    public static final String NEWSTUDENT = "NEWSTUDENT";
    public static final String NEWSTUDENT_GR = "NEWSTUDENT_GR";
    public static final String NEWSTUDENT_UG = "NEWSTUDENT_UG";


    public static final String PARENT = "PARENT";
    public static final String PRESIDENTIAL = "PRESIDENTIAL";
    public static final String SECONDYEAR = "SECONDYEAR";
    public static final String SENIORS = "SENIORS";
    public static final String STUDENT = "STUDENT";
    public static final String TRUSTEE = "TRUSTEE";
    public static final String UACHIEVE_USER = "UACHIEVE_USER";
    public static final String UG_TEMP_INACTIVE = "UG_TEMP_INACTIVE";

    public static final String UNDERGRAD_APPACCEPT = "UNDERGRAD_APPACCEPT";
    public static final String UNDERGRAD_INACTIVE = "UNDERGRAD_INACTIVE";
    public static final String WF_ATHLETICS = "WF_ATHLETICS";
    public static final String WF_CHWP = "WF_CHWP";
    public static final String WF_COUN = "WF_COUN";
    public static final String WF_DATA_CUST_FA = "WF_DATA_CUST_FA";
    public static final String WF_DATA_CUST_LAW_REG = "WF_DATA_CUST_LAW_REG";
    public static final String WF_DATA_CUST_SFS = "WF_DATA_CUST_SFS";
    public static final String WF_DINING_SERVICES = "WF_DINING_SERVICES";
    public static final String WF_DISCIPLINE = "WF_DISCIPLINE";
    public static final String WF_GRAD_OFFICE = "WF_GRAD_OFFICE";
    public static final String WF_HOUSING = "WF_HOUSING";
    public static final String WF_INTL_OFFICE = "WF_INTL_OFFICE";
    public static final String WF_ONE_STOP = "WF_ONE_STOP";
    public static final String WF_OWNER_REG = "WF_OWNER_REG";
    public static final String WF_OWNER_SFS = "WF_OWNER_SFS";
    public static final String WF_PARKING_SERVICES = "WF_PARKING_SERVICES";
    public static final String WF_REGISTRAR = "WF_REGISTRAR";
    public static final String WF_SFS = "WF_SFS";
    public static final String WF_SHC = "WF_SHC";
    public static final String WF_SYSADMIN = "WF_SYSADMIN";

    /*All Active Roles Student */
    public static final String ELA_ACTIVE = "ELA_ACTIVE";
    public static final String FST_ACTIVE = "FST_ACTIVE";
    public static final String GRAD_ACTIVE = "GRAD_ACTIVE";
    public static final String LAW_ACTIVE = "LAW_ACTIVE";
    public static final String MBA_GRAD_ACTIVE = "MBA_GRAD_ACTIVE";
    public static final String ONLINE_ACTIVE = "ONLINE_ACTIVE";
    public static final String PARALEGAL_ACTIVE = "PARALEGAL_ACTIVE";
    public static final String UNDERGRAD_ACTIVE = "UNDERGRAD_ACTIVE";

    /* Portal Admin Role*/
    public static final String PORTAL_ADMIN = "PORTAL_ADMIN";
}
