package edu.sandiego.mysandiegoportal.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by rdhiman on Feb.21.2021.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class User {

    private String firstName;
    private String lastName;
    //private String preferredFirstName;
    private String bannerId;
    private String email;
    private Integer pidm;
    private String username;
    private List<String> roles;
    private List<String> groups = new ArrayList<>();
//    private List<String> holds;
//    private List<String> alerts;



}