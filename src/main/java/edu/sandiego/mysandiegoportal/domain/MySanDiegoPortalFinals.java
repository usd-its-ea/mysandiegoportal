package edu.sandiego.mysandiegoportal.domain;

/**
 * Created by rdhiman on Feb.26.2021.
 */
public class MySanDiegoPortalFinals {
    /** USDSecurity URL. */
    /**
     * USD ERPT API Base URL.
     */
    /*public static final String USDSECURITY_V_1_PERSON = "http://localhost:8080/USDSecurity/v1/banner/person";*/
    public static final String USDSECURITY_V_1_PERSON = "https://mymobiletwolb.sandiego.edu/USDSecurity/v1/banner/person";

    /**  USD ERPT API Base URL.  */
    /*public static final String USD_SECURITY_BASE_URL = "http://localhost:8080/USDSecurity/v1/banner/";*/
    public static final String USD_SECURITY_BASE_URL = "https://mymobiletwolb.sandiego.edu/USDSecurity/v1/banner/";
    /**
     * USD ERPT API Service Path.
     */

    public static final String BYUSERNAME = "byUSDUsername";

    /**  USD ERPT API Service Path.  */
    public static final String USD_SECURITY_FAVORITE_TOOLS_PATH = "favoriteTool/";
    /**  USD Banner Environment.  */
    public static final String USD_BANNER_ENVIRONMENT = "PROD";

    /**
     * This is utility class, hence the private constructor.
     */
    private MySanDiegoPortalFinals() {
    }
}
