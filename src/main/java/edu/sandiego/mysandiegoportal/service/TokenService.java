package edu.sandiego.mysandiegoportal.service;

import edu.sandiego.custom.utils.USDPropertyHelper;
import edu.sandiego.custom.utils.USDSecurityManager;
import edu.sandiego.custom.utils.authentication.AuthenticationService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created by rdhiman on Feb.26.2021.
 */
public class TokenService {
    private static Logger log = LogManager.getLogger(TokenService.class.getName());
    /** USD Security token. */
    private String usdSecurityToken = null;

    /** USDSecurity Manager. */
    private USDSecurityManager manager = null;

    /** AuthenticationService. */
    private AuthenticationService authService = null;

    /*  Security related behaviors. */

    /** Get USD Security token.
     * @return token string.
     */
    public String getUsdSecurityToken() {
        if (usdSecurityToken == null) {
            refreshSecurityToken();
        }
        return usdSecurityToken;
    }

    /**
     * Set USD Security token.
     * @param token token string.
     */
    private void setUsdSecurityToken(final String token) {
        this.usdSecurityToken = token;
    }

    /** Refresh USD Security token. */
    private void refreshSecurityToken() {
        USDPropertyHelper helper = USDPropertyHelper.getInstance();
        AuthenticationService svc = getAuthService();

        // Generate a token using the authService.
        String token = svc.getUSDSecurityToken(
                helper.getUSDSecurityClientID(),
                helper.getUSDSecuritySecret(),
                helper.getUSDSecurityBannerUser(),
                helper.getUSDSecurityBannerPassword(),
                "dev"
        );

        // Set the token.
        setUsdSecurityToken(token);
    }

    /**
     * Get USD Security Manager singleton.
     * @return USDSecurityManager Singleton instance.
     */
    private USDSecurityManager getManager() {
        if (manager == null) {
            setManager(USDSecurityManager.getInstance());
        }
        return manager;
    }

    /**
     * Set USDSecurity Manager.
     * @param m USDSecurityManager.
     */
    private void setManager(final USDSecurityManager m) {
        this.manager = m;
    }

    /**
     * Get Auth Service.
     * @return AuthenticationService.
     */
    private AuthenticationService getAuthService() {
        USDSecurityManager mgr = getManager();
        if (authService == null) {
            authService = mgr.getAuthenticationService();
        }
        return authService;
    }
}

