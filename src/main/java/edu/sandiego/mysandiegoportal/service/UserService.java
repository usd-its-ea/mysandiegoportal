package edu.sandiego.mysandiegoportal.service;

import edu.sandiego.mysandiegoportal.domain.PersonDetailedInfo;
import org.springframework.stereotype.Service;

/**
 * Created by rdhiman on Feb.23.2021.
 */
@Service
public interface UserService {

    public PersonDetailedInfo getUserByUsername(String username);


}
