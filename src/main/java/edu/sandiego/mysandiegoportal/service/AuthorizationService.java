package edu.sandiego.mysandiegoportal.service;

import edu.sandiego.mysandiegoportal.domain.PersonDetailedInfo;
import org.springframework.stereotype.Service;

import java.util.List;

import static edu.sandiego.mysandiegoportal.domain.GroupNames.*;
import static edu.sandiego.mysandiegoportal.domain.URI.*;

/**
 * Created by rdhiman on Apr.05.2021.
 */
@Service("AuthorizationService")
public class AuthorizationService {

    public Boolean isUserAuthorized(PersonDetailedInfo personDetailedInfo, String requestURI) {
        List<String> groupList = personDetailedInfo.getBasicPersonInfo().getGroups();
        Boolean isAuthorized = false;
        /* Employee may have access to pages in different tabs.
           1. Check if user.employee has access to any of that pages
           2. Narrow it down to page level.
        *    */
        /* ###################### Employee Authorization ######################
            #####################                        ###################### */
        if (requestURI.contains(EMPLOYEE_URI)) {
            /* Breaking further down into page level authorization. */
            if (requestURI.contains("teach")
                    || requestURI.contains("advise")
                    || requestURI.contains("home")
                    || requestURI.contains("pushnotifications")
                    || requestURI.contains("tools")
                    || requestURI.contains("applicationsAndTools")
                    || requestURI.contains("facultynewsnotes")
                    || requestURI.contains("heatday")
                    || requestURI.contains("workday")
                    || requestURI.contains("employeeresources")
                    || requestURI.contains("trainingsandworkshops")
                    || requestURI.contains("proposalreview")
                    || requestURI.endsWith("employee/")) {
                if (groupList.contains(EMPLOYEE_GROUP))
                    isAuthorized = true;
            } else if (requestURI.contains("departmentAndChair")) {
                if (groupList.contains(DEPT_CHAIR_GROUP))
                    isAuthorized = true;
            }
            return isAuthorized;
            /* ###################### Undergrad Authorization ######################
            #########################                   ###################### */
        } else if (requestURI.contains(UG_URI)) {
            /* Breaking further down into page level authorization.*/
            if (requestURI.contains("home") || requestURI.contains("applicant")) {
                if (groupList.contains(UG_ADMISSIONS_TAB_GROUP)) {
                    isAuthorized = true;
                }
            } else if (requestURI.contains("admitted")) {
                if (groupList.contains(UG_DEPOSITED_AND_ADMITTED_GROUP))
                    isAuthorized = true;
            } else if (requestURI.contains("deposited")) {
                if (groupList.contains(UG_DEPOSITED_GROUP))
                    isAuthorized = true;
            } else if (requestURI.contains("newstudent")) {
                if (groupList.contains(UG_NEWSTUDENT_GROUP))
                    isAuthorized = true;
            } else if (requestURI.contains("clubsandactivities")) {
                if (groupList.contains(UNDERGRAD_ACTIVE_GROUP))
                    isAuthorized = true;
            } else if (requestURI.contains("ugresearch")) {
                if (groupList.contains(UNDERGRAD_ACTIVE_GROUP))
                    isAuthorized = true;
            }
            return isAuthorized;
            /* ###################### Grad Authorization ######################
            #########################                   ###################### */
        } else if (requestURI.contains(GRAD_URI)) {
            /* Breaking further down into page level authorization.*/
            if (requestURI.contains("home") || requestURI.contains("applicant")) {
                if (groupList.contains(GRAD_ADMISSIONS_TAB_GROUP)) {
                    isAuthorized = true;
                }
            } else if (requestURI.contains("admitted")) {
                if (groupList.contains(GRAD_DEPOSITED_AND_ADMITTED_GROUP))
                    isAuthorized = true;
            } else if (requestURI.contains("deposited")) {
                if (groupList.contains(GRAD_DEPOSITED_GROUP))
                    isAuthorized = true;
            } else if (requestURI.contains("gradlife")) {
                if (groupList.contains(GRAD_ACTIVE_GROUP))
                    isAuthorized = true;
            }
            return isAuthorized;
            /* ###################### Law Authorization ######################
            #########################                   ###################### */
        } else if (requestURI.contains(LAW_URI)) {
            /* Breaking further down into page level authorization.*/
            if (requestURI.contains("admissions")) {
                if (groupList.contains(LAW_ADMISSIONS_TAB_GROUP))
                    isAuthorized = true;
            } else if (requestURI.contains("admissions")) {
                if (groupList.contains(LAW_ACTIVE_GROUP))
                    isAuthorized = true;
            } else if (requestURI.contains("paralegal")) {
                if (groupList.contains(PARALEGAL_ACTIVE_GROUP))
                    isAuthorized = true;
            }
            return isAuthorized;
            /* ###################### FST Authorization ######################
            #########################                   ###################### */
        } else if (requestURI.contains(FST_URI)) {
            /* Breaking further down into page level authorization.*/
            if (requestURI.contains("academics")
                    || requestURI.contains("finaid-studentaccounts")
                    || requestURI.contains("usdservices")
                    || requestURI.contains("studentlife")) {
                if (groupList.contains(FST_STUDENT_TAB_GROUP))
                    isAuthorized = true;
            }
            return isAuthorized;
            /* ###################### ELA Authorization ######################
            #########################                   ###################### */
        } else if (requestURI.contains(ELA_URI)) {
            /* Breaking further down into page level authorization.*/
            if (requestURI.contains("ela")) {
                if (groupList.contains(ELA_ACTIVE_GROUP))
                    isAuthorized = true;
            }
            return isAuthorized;
            /* ###################### Alumni Authorization ######################
            #########################                   ###################### */
        } else if (requestURI.contains(ALUMNI_URI)) {
            /* Breaking further down into page level authorization.*/
            if (requestURI.contains("mystudentinfo") || requestURI.contains("alumni")) {
                if (groupList.contains(ALUMNI_TAB_GROUP))
                    isAuthorized = true;
            }
            return isAuthorized;
            /* ###################### Torero Life Authorization ######################
            #########################                   ###################### */
        } else if (requestURI.contains(TORERO_LIFE_URI)) {
            /* Breaking further down into page level authorization.*/
            if (requestURI.contains("studentaffairs")) {
                if (groupList.contains(TORERO_LIFE_TAB_GROUP))
                    isAuthorized = true;
            } else if (requestURI.contains("athletics")) {
                if (groupList.contains(TORERO_LIFE_TAB_GROUP))
                    isAuthorized = true;
            } else if (requestURI.contains("events-ole")) {
                if (groupList.contains(ALL_ACTIVE_STUDENTS_GROUP))
                    isAuthorized = true;
            } else if (requestURI.contains("pacificridge")) {
                if (groupList.contains(TORERO_LIFE_TAB_GROUP))
                    isAuthorized = true;
            }
            return isAuthorized;
            /* ###################### Help Desk & Support Authorization ######################
            #########################                   ###################### */
        } else if (requestURI.contains(HELP_DESK_AND_SUPPORT_URI)) {
            /* Breaking further down into page level authorization.*/
            if (requestURI.contains("mysupportservices")) {
                if (groupList.contains(MY_SUPPORT_SERVICES_PAGE_GROUP))
                    isAuthorized = true;
            } else if (requestURI.contains("safetycheck")) {
                if (groupList.contains(SAFETY_CHECK_PAGE_GROUP))
                    isAuthorized = true;
            }
            return isAuthorized;
            /* ###################### Common Pages Authorization ######################
            #########################                   ###################### */
        } else if (requestURI.contains(COMMON_URI)) {
            /* Breaking further down into page level authorization.*/
            if (requestURI.contains("myacademics")) {
                if (groupList.contains(GRAD_OR_UNDERGRAD_ACTIVE_GROUP))
                    isAuthorized = true;
            } else if (requestURI.contains("financialaid")) {
                if (groupList.contains(GRAD_OR_UNDERGRAD_ACTIVE_GROUP))
                    isAuthorized = true;
            } else if (requestURI.contains("studentemployment")) {
                if (groupList.contains(STUDENT_EMPLOYMENT_PAGE_GROUP))
                    isAuthorized = true;
            }
            return isAuthorized;
            /* ###################### Applications & Tools Authorization ######################
            #########################                   ###################### */
        }/*else if (requestURI.contains(APPLICATIONS_AND_TOOLS_URI)) {
         *//* Breaking further down into page level authorization.*//*
         *//* Authorization for all pages under this tab has been taken care by Student and Employee Authorization above.*//*
        }*/
        return isAuthorized;
    }
}
