package edu.sandiego.mysandiegoportal.service.employee;

import edu.sandiego.mysandiegoportal.domain.MySanDiegoPortalFinals;
import edu.sandiego.mysandiegoportal.service.PropertyFilesService;
import edu.sandiego.mysandiegoportal.service.TokenService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.net.ssl.SSLContext;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.FileReader;
import java.io.Reader;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

import static edu.sandiego.mysandiegoportal.domain.ApplicationFinals.EMPLOYEE_FAVORITE_APPLICATIONS_PROPERTY_NAME;
import static edu.sandiego.mysandiegoportal.domain.ApplicationFinals.MYSANDIEGOPORTAL_CONFIG_FILE_FULL_PATH;
import static edu.sandiego.mysandiegoportal.domain.MySanDiegoPortalFinals.*;

/**
 * Created by rdhiman on Mar.23.2021.
 */
@Service("EmployeeService")
public class EmployeeService {

    private static Logger log = LogManager.getLogger(EmployeeService.class.getName());
    @Autowired
    PropertyFilesService propertyFilesService;

    public String getToken() {
        TokenService tokenService = new TokenService();
        String token = tokenService.getUsdSecurityToken();
        return token;
    }

    /*Load Applications and Tools information from JSON File:
    Saved in the location env variable MYSANDIEGOPORTAL_CONFIG_LOCATION */
    public JSONArray loadApplicationsAndToolsFromJSONFile() {
        try {
            JSONParser parser = new JSONParser();
            // JSON Array of All Applications and Tools (Got from Json file)
            JSONArray allApplicationAndToolsJsonArray = new JSONArray();
            log.info("######################################################################################");
            log.info("MYSANDIEGO PORTAL CONFIG FILE FULL PATH: " + MYSANDIEGOPORTAL_CONFIG_FILE_FULL_PATH);
            log.info(("Employee Favorit Applications Location:-> " + propertyFilesService.getPropertyValue(EMPLOYEE_FAVORITE_APPLICATIONS_PROPERTY_NAME)));
            Reader reader = new FileReader(propertyFilesService.getPropertyValue(EMPLOYEE_FAVORITE_APPLICATIONS_PROPERTY_NAME));
            // JSON Array of All Applications and Tools (Got from Json file)
            JSONObject jsonObject = (JSONObject) parser.parse(reader);
            // loop through the array
            JSONArray favoriteAppsJSON = (JSONArray) jsonObject.get("applications");
            for (int i = 0; i < favoriteAppsJSON.size(); i++) {
                JSONObject jsonObject1 = (JSONObject) favoriteAppsJSON.get(i);
                allApplicationAndToolsJsonArray.add(jsonObject1);
            }
            return allApplicationAndToolsJsonArray;
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Unknown exception: " + e.getMessage());
        }
        return null;
    }

    public JSONArray getMyFavAppsIdFromBanner(String username) {
        String token = getToken();
        assert token != null : "Token was null.";
        try {
            //Converting the Object to JSONString
            //String jsonString = mapper.writeValueAsString(usdOneUserInfo);
            // JSON Array of all MY favorite apps and tools (got from web-service)
            JSONArray myFavoriteAppsAndToolsJsonArray = new JSONArray();
            Response responseEntity = null;
            SSLContext context = SSLContext.getInstance("TLSv1.2");
            context.init(null, null, null);
            Client client =
                    ClientBuilder.newBuilder().sslContext(context).build();
            responseEntity =
                    client
                            .target(MySanDiegoPortalFinals.USD_SECURITY_BASE_URL)
                            .path(USD_SECURITY_FAVORITE_TOOLS_PATH)
                            .queryParam("environmentType", USD_BANNER_ENVIRONMENT)
                            .queryParam("username", username)
                            .request(MediaType.APPLICATION_JSON)
                            .header("Authorization", "Bearer " + token)
                            .get();
            String jsonResponse = responseEntity.readEntity(String.class);
            JSONParser jsonParser = new JSONParser();
            Object resultObject = jsonParser.parse(jsonResponse);
            return myFavoriteAppsAndToolsJsonArray = (JSONArray) resultObject;
        } catch (NoSuchAlgorithmException e) {

            log.error("NoSuchAlgorithmException: " + e.getMessage());
        } catch (KeyManagementException e) {

            log.error("KeyManagementException: " + e.getMessage());
        } catch (ClassCastException e2) {

            log.error("Received class cast exception. Likely cause is that the user passed an invalid Salesforce token:"
                    + e2.getMessage());
        } catch (Exception e1) {

            log.error("Unknown exception: " + e1.getMessage());
        }
        return null;
    }

    public String saveMyFavoritesApplicationsPreferences(
            String username, String appId, String appName, String appDescription, String appUrl, String appCategory,
            String isFavorite, String targetAction) {

        String token = getToken();
        assert token != null : "Token was null.";

        try {
            Form form = new Form();
            form.param("environmentType", "PROD");
            form.param("username", username);
            form.param("appId", appId);
            form.param("appName", appName);
            form.param("appDescription", appDescription);
            form.param("appURL", appUrl);
            form.param("appCategory", appCategory);
            form.param("isFavorite", isFavorite);
            //Converting the Object to JSONString
            //String jsonString = mapper.writeValueAsString(usdOneUserInfo);
            // JSON Array of all MY favorite apps and tools (got from web-service)
            JSONArray myFavoriteAppsAndToolsJsonArray = new JSONArray();
            Response responseEntity = null;
            SSLContext context = SSLContext.getInstance("TLSv1.2");
            context.init(null, null, null);
            Client client =
                    ClientBuilder.newBuilder().sslContext(context).build();
            if (targetAction.equalsIgnoreCase("save")) {
                responseEntity =
                        client
                                .target(USD_SECURITY_BASE_URL)
                                .path(USD_SECURITY_FAVORITE_TOOLS_PATH)
                                .request(MediaType.APPLICATION_JSON)
                                .header("Authorization", "Bearer " + token)
                                .post(Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED_TYPE), Response.class);

            } else if (targetAction.equalsIgnoreCase("delete")) {
                responseEntity =
                        client
                                .target(USD_SECURITY_BASE_URL)
                                .path(USD_SECURITY_FAVORITE_TOOLS_PATH)
                                .queryParam("environmentType", "PROD")
                                .queryParam("username", username)
                                .queryParam("appId", appId)
                                .queryParam("isFavorite", isFavorite)
                                .request(MediaType.APPLICATION_JSON)
                                .header("Authorization", "Bearer " + token)
                                .delete();
                String stringResponse = responseEntity.readEntity(String.class);
                return stringResponse;
            }
        } catch (NoSuchAlgorithmException e) {

            log.error("NoSuchAlgorithmException: " + e.getMessage());
        } catch (KeyManagementException e) {

            log.error("KeyManagementException: " + e.getMessage());
        } catch (ClassCastException e2) {

            log.error("Received class cast exception. Likely cause is that the user passed an invalid Salesforce token:"
                    + e2.getMessage());
        } catch (Exception e1) {

            log.error("Unknown exception: " + e1.getMessage());
        }
        return null;
    }

}
