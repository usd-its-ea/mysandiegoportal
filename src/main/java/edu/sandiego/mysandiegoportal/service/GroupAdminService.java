package edu.sandiego.mysandiegoportal.service;

import edu.sandiego.mysandiegoportal.domain.PersonDetailedInfo;
import edu.sandiego.mysandiegoportal.domain.User;
import org.springframework.stereotype.Service;

import static edu.sandiego.mysandiegoportal.domain.GroupNames.*;
import static edu.sandiego.mysandiegoportal.domain.RoleNames.*;

/**
 * Created by rdhiman on Feb.22.2021.
 */
@Service
public class GroupAdminService {


    public PersonDetailedInfo setMyGroups(PersonDetailedInfo personDetailedInfo) {

        User user = personDetailedInfo.getBasicPersonInfo();

        /*###############################################################################
           Group Expressions by Roles.
          ###############################################################################*/

        /* Portal Admin Group -> Seen by Portal Admins + any support roles.*/
        Boolean PORTAL_ADMIN_GROUP_EXPRESSION = user.getRoles().contains(PORTAL_ADMIN);

            /*-------------------------------------------------------------------------------
               Group Expressions by Roles:  Students Related Roles Start
              -------------------------------------------------------------------------------*/

        /*Not Applicant Group, anyone who is Not(APPLICANT_UG OR APPLICANT_GRAD) */
        Boolean NOT_APPLICANT_GROUP_EXPRESSION = (!(user.getRoles().contains(APPLICANT_UG)
                || user.getRoles().contains(APPLICANT_GRAD))
                || (PORTAL_ADMIN_GROUP_EXPRESSION));

        /*Grad OR Undergrad Active Group - > Seen by Active Grad or Active UG Students only.*/
        Boolean GR_OR_UG_ACTIVE_GROUP_EXPRESSION = user.getRoles().contains(GRAD_ACTIVE)
                || user.getRoles().contains(UNDERGRAD_ACTIVE)
                || (PORTAL_ADMIN_GROUP_EXPRESSION);

        /*Applicant Group Expression -> Seen by Grad OR UG Applicants only.*/
        Boolean ALL_APPLICANTS_GROUP_EXPRESSION = user.getRoles().contains(APPLICANT_GRAD)
                || user.getRoles().contains(APPLICANT_UG)
                || (PORTAL_ADMIN_GROUP_EXPRESSION);

        /* This includes Any Active Student Roles + Seniors.
         * Alumni is not considered in this group. */
        Boolean ALL_ACTIVE_STUDENTS_GROUP_EXPRESSION = user.getRoles().contains(GRAD_ACTIVE)
                || user.getRoles().contains(UNDERGRAD_ACTIVE)
                || user.getRoles().contains(ELA_ACTIVE)
                || user.getRoles().contains(LAW_ACTIVE)
                || user.getRoles().contains(FST_ACTIVE)
                || user.getRoles().contains(ONLINE_ACTIVE)
                || user.getRoles().contains(PARALEGAL_ACTIVE)
                || user.getRoles().contains(MBA_GRAD_ACTIVE)
                || user.getRoles().contains(SENIORS)
                || (PORTAL_ADMIN_GROUP_EXPRESSION);

        Boolean ALL_ACTIVE_STUDENTS_AND_ALL_APPLICANTS_GROUP_EXPRESSION = user.getRoles().contains(GRAD_ACTIVE)
                || user.getRoles().contains(UNDERGRAD_ACTIVE)
                || user.getRoles().contains(ELA_ACTIVE)
                || user.getRoles().contains(LAW_ACTIVE)
                || user.getRoles().contains(FST_ACTIVE)
                || user.getRoles().contains(ONLINE_ACTIVE)
                || user.getRoles().contains(PARALEGAL_ACTIVE)
                || user.getRoles().contains(MBA_GRAD_ACTIVE)
                || user.getRoles().contains(SENIORS)
                || ALL_APPLICANTS_GROUP_EXPRESSION
                || (PORTAL_ADMIN_GROUP_EXPRESSION);

        /* Online Active Group -> Seen by Online_Active + any support roles.*/
        Boolean ONLINE_ACTIVE_GROUP_EXPRESSION = user.getRoles().contains(ONLINE_ACTIVE)
                || (PORTAL_ADMIN_GROUP_EXPRESSION);

        /* MBA Grad Active Group -> Seen by MBA_Grad_Active + any support roles.*/
        Boolean MBA_GRAD_ACTIVE_GROUP_EXPRESSION = user.getRoles().contains(MBA_GRAD_ACTIVE)
                || (PORTAL_ADMIN_GROUP_EXPRESSION);

        /* ELA Active Group -> Seen by Ela Active + any support roles.*/
        Boolean ELA_ACTIVE_GROUP_EXPRESSION = user.getRoles().contains(ELA_ACTIVE)
                || (PORTAL_ADMIN_GROUP_EXPRESSION);

        /* Law Active Group -> Seen by Law Active + any support roles.*/
        Boolean LAW_ACTIVE_GROUP_EXPRESSION = user.getRoles().contains(LAW_ACTIVE)
                || (PORTAL_ADMIN_GROUP_EXPRESSION);

        /* Grad Active Group -> Seen by Grad Active + any support roles.*/
        Boolean GRAD_ACTIVE_GROUP_EXPRESSION = user.getRoles().contains(GRAD_ACTIVE)
                || (PORTAL_ADMIN_GROUP_EXPRESSION);

        /* UG Active Group -> Seen by uG Active + any support roles.*/
        Boolean UNDERGRAD_ACTIVE_GROUP_EXPRESSION = user.getRoles().contains(UNDERGRAD_ACTIVE)
                || (PORTAL_ADMIN_GROUP_EXPRESSION);

        /*Paralegal Active Group -> Seen by Paralegal_active + any support roles.*/
        Boolean PARALEGAL_ACTIVE_GROUP_EXPRESSION = user.getRoles().contains(PARALEGAL_ACTIVE)
                || (PORTAL_ADMIN_GROUP_EXPRESSION);

        /********************* Law Related Groups Expressions. */

        /* Law Admitted JD Groip Expression -> Seen by LAW_ADMITTED_JD */
        Boolean LAW_ADMITTED_JD_GROUP_EXPRESSION = user.getRoles().contains(LAW_ADMITTED_JD);
        /* Law Admitted JD Groip Expression -> Seen by LAW_ADMITTED_LLM */
        Boolean LAW_ADMITTED_LLM_GROUP_EXPRESSION = user.getRoles().contains(LAW_ADMITTED_LLM);

        /* Alumni Group -> Seen by Portal Admins + any support roles.*/
        Boolean ALUMNI_GROUP_EXPRESSION = user.getRoles().contains(ALUMNI)
                || (PORTAL_ADMIN_GROUP_EXPRESSION);

        /* Seniors Group -> Seen by Seniors + any support roles.*/
        Boolean SENIOR_GROUP_EXPRESSION = user.getRoles().contains(SENIORS)
                || (PORTAL_ADMIN_GROUP_EXPRESSION);
             /*-------------------------------------------------------------------------------
                        Group Expressions by Roles:  Students Related Roles End
              -------------------------------------------------------------------------------*/

            /*-------------------------------------------------------------------------------
                            Group Expressions by Roles:  Employee Related Roles Start
             -------------------------------------------------------------------------------*/

        /* Employee Group -> Seen by Employee + any support roles.*/
        Boolean EMPLOYEE_GROUP_EXPRESSION = user.getRoles().contains(EMPLOYEE)
                || (PORTAL_ADMIN_GROUP_EXPRESSION);

        /* Department Chair Group -> Seen by Department Chair + any support roles.*/
        Boolean DEPARTMENT_CHAIR_GROUP_EXPRESSION = user.getRoles().contains(DEPT_CHAIR)
                || (PORTAL_ADMIN_GROUP_EXPRESSION);


        /* Dept Chair  Group -> Seen by DEPOSITED_UG + any support roles.*/
        Boolean DEPT_CHAIR_GROUP_EXPRESSION = user.getRoles().contains(DEPT_CHAIR)
                || (PORTAL_ADMIN_GROUP_EXPRESSION);

            /*-------------------------------------------------------------------------------
                            Group Expressions by Roles:  Employee Related Roles End
             -------------------------------------------------------------------------------*/


            /*-------------------------------------------------------------------------------
              Group Expressions by Roles:
                    Different combination of Admissions Tab Related Roles: GR, UG, LAW
                                                Start
              -------------------------------------------------------------------------------*/

        /* Grad Applicant  Group -> Seen by Applicant_Grad + any support roles.*/
        Boolean GRAD_APPLICANT_GROUP_EXPRESSION = user.getRoles().contains(APPLICANT_GRAD)
                || (PORTAL_ADMIN_GROUP_EXPRESSION);

        /* Grad Admitted  Group -> Seen by ADMITTED_GR + any support roles.*/
        Boolean GRAD_ADMITTED_GROUP_EXPRESSION = user.getRoles().contains(ADMITTED_GR)
                || (PORTAL_ADMIN_GROUP_EXPRESSION);

        /* Grad Deposited  Group -> Seen by DEPOSITED_GR + any support roles.*/
        Boolean GRAD_DEPOSITED_GROUP_EXPRESSION = user.getRoles().contains(DEPOSITED_GR)
                || (PORTAL_ADMIN_GROUP_EXPRESSION);

        /* Grad Deposited Group -> Seen by Grad*/
        Boolean GRAD_DEPOSITED_AND_ADMITTED_GROUP_EXPRESSION = (user.getRoles().contains(DEPOSITED_GR)
                || user.getRoles().contains(ADMITTED_GR)
                || PORTAL_ADMIN_GROUP_EXPRESSION);

        /* Undergrad Applicant  Group -> Seen by APPLICANT_UG + any support roles.*/
        Boolean UG_APPLICANT_GROUP_EXPRESSION = user.getRoles().contains(APPLICANT_UG)
                || (PORTAL_ADMIN_GROUP_EXPRESSION);

        /* Undergrad Admitted  Group -> Seen by ADMITTED_UG + any support roles.*/
        Boolean UG_ADMITTED_GROUP_EXPRESSION = user.getRoles().contains(ADMITTED_UG)
                || (PORTAL_ADMIN_GROUP_EXPRESSION);

        /* Undergrad Deposited  Group -> Seen by DEPOSITED_UG + any support roles.*/
        Boolean UG_DEPOSITED_GROUP_EXPRESSION = user.getRoles().contains(DEPOSITED_UG)
                || (PORTAL_ADMIN_GROUP_EXPRESSION);

        Boolean UG_DEPOSITED_AND_ADMITTED_GROUP_EXPRESSION = (user.getRoles().contains(DEPOSITED_UG)
                || user.getRoles().contains(ADMITTED_UG)
                || PORTAL_ADMIN_GROUP_EXPRESSION);

        Boolean UG_NEWSTUDENT_GROUP_EXPRESSION = (user.getRoles().contains(NEWSTUDENT_UG)
                || PORTAL_ADMIN_GROUP_EXPRESSION);

        Boolean ALL_ADMITTED_GROUP_EXPRESSION = (GRAD_ADMITTED_GROUP_EXPRESSION
                || UG_ADMITTED_GROUP_EXPRESSION
                || LAW_ADMITTED_JD_GROUP_EXPRESSION
                || LAW_ADMITTED_LLM_GROUP_EXPRESSION
                || PORTAL_ADMIN_GROUP_EXPRESSION);


            /*-------------------------------------------------------------------------------
              Group Expressions by Roles:
                    Different combination of Admissions Tab Related Roles: GR, UG, LAW
                                                End
              -------------------------------------------------------------------------------*/

         /*###############################################################################
            Group Expressions for Tabs on left hand menu.
          ###############################################################################*/


        /* Torero Hub Tab -> Seen by everyone but Applicants And Alumni*/
        Boolean TOREROHUB_TAB_GROUP_EXPRESSION = (ALL_ACTIVE_STUDENTS_GROUP_EXPRESSION
                || EMPLOYEE_GROUP_EXPRESSION
                || PORTAL_ADMIN_GROUP_EXPRESSION);

        /*Faculty & Dept Chairs Tab-> Seen by Employee Or Dept Chairs Only */
        Boolean FACULTY_AND_DEPT_CHAIRS_TAB_GROUP_EXPRESSION = (DEPARTMENT_CHAIR_GROUP_EXPRESSION
                || EMPLOYEE_GROUP_EXPRESSION
                || PORTAL_ADMIN_GROUP_EXPRESSION);

        /* Employee Tab -> Seen by Employee Only*/
        Boolean EMPLOYEE_TAB_GROUP_EXPRESSION = EMPLOYEE_GROUP_EXPRESSION || PORTAL_ADMIN_GROUP_EXPRESSION;

        /*Help Desk & Support Tab -> Seen by ALL who can login to Portal.*/
        Boolean HELP_DESK_AND_SUPPORT_TAB_GROUP_EXPRESSION = true;

        /*Torero Life Tab -> Seen by everyone but Law_Active*/
        Boolean TORERO_LIFE_TAB_GROUP_EXPRESSION = (user.getRoles().contains(UNDERGRAD_ACTIVE)
                || user.getRoles().contains(GRAD_ACTIVE)
                || user.getRoles().contains(ELA_ACTIVE)
                || user.getRoles().contains(ONLINE_ACTIVE)
                || user.getRoles().contains(PARALEGAL_ACTIVE)
                || user.getRoles().contains(MBA_GRAD_ACTIVE)
                || user.getRoles().contains(ALUMNI)
                || user.getRoles().contains(EMPLOYEE)
                || PORTAL_ADMIN_GROUP_EXPRESSION)
                && (!user.getRoles().contains(LAW_ACTIVE));

        /* FST Student Tab -> Seen by all FST_ACTIVE + SUPPORT_FST_PORTAL.*/
        Boolean FST_STUDENT_TAB_GROUP_EXPRESSIONS = user.getRoles().contains("FST_ACTIVE")
                || PORTAL_ADMIN_GROUP_EXPRESSION;

        /* Law Admissions Tab -> Seen by all JD and LLM*/
        Boolean LAW_ADMISSIONS_TAB_GROUP_EXPRESSION = user.getRoles().contains(LAW_ADMITTED_JD)
                || user.getRoles().contains(LAW_ADMITTED_LLM)
                || user.getRoles().contains(LAW_DEPOSIT1_JD)
                || user.getRoles().contains(LAW_DEPOSIT2_JD)
                || user.getRoles().contains(LAW_DEPOSIT_LLM)
                || user.getRoles().contains(LAW_ADMITTED_TRANSFER_JD)
                || user.getRoles().contains(LAW_ADMITTED_VISITOR_JD)
                || PORTAL_ADMIN_GROUP_EXPRESSION;

        /* Graduate Admissions Tab - > GR Admitted + GR Deposited + GR Applicant.*/
        Boolean GRAD_ADMISSIONS_TAB_GROUP_EXPRESSION = user.getRoles().contains(ADMITTED_GR)
                || user.getRoles().contains(APPLICANT_GRAD)
                || user.getRoles().contains(DEPOSITED_GR)
                || PORTAL_ADMIN_GROUP_EXPRESSION;

        /* UG Admissions Tab - > GR Admitted + GR Deposited + GR Applicant.*/
        Boolean UG_ADMISSIONS_TAB_GROUP_EXPRESSION = user.getRoles().contains(ADMITTED_UG)
                || user.getRoles().contains(APPLICANT_UG)
                || user.getRoles().contains(DEPOSITED_UG)
                || PORTAL_ADMIN_GROUP_EXPRESSION;

        /* Torero Alumni Tab- > seen by users with Alumni Role.*/
        Boolean ALUMNI_TAB_GROUP_EXPRESSION = ALUMNI_GROUP_EXPRESSION || PORTAL_ADMIN_GROUP_EXPRESSION;

        /* Applications And Tools Tab- > seen by users with following Roles' combination.*/
        Boolean APPLICATIONS_AND_TOOLS_TAB_GROUP_EXPRESSION = ALUMNI_GROUP_EXPRESSION
                || EMPLOYEE_GROUP_EXPRESSION
                || ALL_ACTIVE_STUDENTS_GROUP_EXPRESSION
                || PORTAL_ADMIN_GROUP_EXPRESSION;


        /*###############################################################################
            Group Expressions by Pages under Tabs.
          ###############################################################################*/

        Boolean STUDENT_EMPLOYMENT_PAGE_GROUP_EXPRESSION = GRAD_ACTIVE_GROUP_EXPRESSION
                || UNDERGRAD_ACTIVE_GROUP_EXPRESSION
                || ELA_ACTIVE_GROUP_EXPRESSION
                || EMPLOYEE_GROUP_EXPRESSION
                || ONLINE_ACTIVE_GROUP_EXPRESSION
                || PARALEGAL_ACTIVE_GROUP_EXPRESSION
                || MBA_GRAD_ACTIVE_GROUP_EXPRESSION
                || PORTAL_ADMIN_GROUP_EXPRESSION;

        Boolean USDONE_PAGE_GROUP_EXPRESSION = (EMPLOYEE_GROUP_EXPRESSION
                || ALL_ACTIVE_STUDENTS_GROUP_EXPRESSION
                || ALUMNI_GROUP_EXPRESSION
                || PORTAL_ADMIN_GROUP_EXPRESSION)
                && (NOT_APPLICANT_GROUP_EXPRESSION);

        Boolean MY_SUPPORT_SERVICES_PAGE_GROUP_EXPRESSION = (EMPLOYEE_GROUP_EXPRESSION
                || ALL_ACTIVE_STUDENTS_GROUP_EXPRESSION
                || ALUMNI_GROUP_EXPRESSION
                || PORTAL_ADMIN_GROUP_EXPRESSION
                || ALL_APPLICANTS_GROUP_EXPRESSION);

        Boolean SAFETY_CHECK_PAGE_GROUP_EXPRESSION = (EMPLOYEE_GROUP_EXPRESSION
                || ALL_ACTIVE_STUDENTS_GROUP_EXPRESSION
                || ALUMNI_GROUP_EXPRESSION
                || PORTAL_ADMIN_GROUP_EXPRESSION);

        Boolean EVENTS_PAGE_GROUP_EXPRESSION = (ALL_ADMITTED_GROUP_EXPRESSION
                || NOT_APPLICANT_GROUP_EXPRESSION
                || PORTAL_ADMIN_GROUP_EXPRESSION);

         /*###############################################################################
            Add Groups to User Object  w.r.t Roles as applicable.
          ###############################################################################*/

        /* Add Applicant Group If Applicant AND Not Employee.*/
        if ((ALL_APPLICANTS_GROUP_EXPRESSION) && (!EMPLOYEE_GROUP_EXPRESSION)) {
            user.getGroups().add(APPLICANT_GROUP);
        }
        /* UG Active Group .*/
        if (UNDERGRAD_ACTIVE_GROUP_EXPRESSION) {
            user.getGroups().add(UNDERGRAD_ACTIVE_GROUP);
        }
        /* Grad Active Group */
        if (GRAD_ACTIVE_GROUP_EXPRESSION) {
            user.getGroups().add(GRAD_ACTIVE_GROUP);
        }
        /* Employee Group */
        if (EMPLOYEE_GROUP_EXPRESSION) {
            user.getGroups().add(EMPLOYEE_GROUP);
        }
        /* Admin Group */
        if (PORTAL_ADMIN_GROUP_EXPRESSION) {
            user.getGroups().add(PORTAL_ADMIN_GROUP);
        }
        /* Law Group */
        if (LAW_ACTIVE_GROUP_EXPRESSION) {
            user.getGroups().add(LAW_ACTIVE_GROUP);
        }
        /* Ela Group */
        if (ELA_ACTIVE_GROUP_EXPRESSION) {
            user.getGroups().add(ELA_ACTIVE_GROUP);
        }
        /* Paralegal Active Group */
        if (PARALEGAL_ACTIVE_GROUP_EXPRESSION) {
            user.getGroups().add(PARALEGAL_ACTIVE_GROUP);
        }
        /* Senior Group */
        if (SENIOR_GROUP_EXPRESSION) {
            user.getGroups().add(SENIOR_GROUP);
        }
        /* Dept Chair Group */
        if (DEPARTMENT_CHAIR_GROUP_EXPRESSION) {
            user.getGroups().add(DEPT_CHAIR_GROUP);
        }
        /* Grad OR Undergrad Active group:
         *  If personDetailedInfo has GRAD_ACTIVE OR UNDERGRAD_ACTIVE */
        if (GR_OR_UG_ACTIVE_GROUP_EXPRESSION) {
            user.getGroups().add(GRAD_OR_UNDERGRAD_ACTIVE_GROUP);
        }
        /*ALL_ACTIVE_STUDENTS_GROUP:
         * All students who have _Active in their role. */
        if (ALL_ACTIVE_STUDENTS_GROUP_EXPRESSION) {
            user.getGroups().add(ALL_ACTIVE_STUDENTS_GROUP);
        }
        /* Anyone but Applicant Group:
         * This Group contains any Active Students + Employee + Support_Roles(if any) But Not Applicants. */
        if ((ALL_ACTIVE_STUDENTS_GROUP_EXPRESSION) && (NOT_APPLICANT_GROUP_EXPRESSION)) {
            user.getGroups().add(EVERYONE_BUT_APPLICANTS_GROUP);
        }
        /*Add Admissions Tab  Related Groups: GR, UG*/

        if (GRAD_APPLICANT_GROUP_EXPRESSION) {
            user.getGroups().add(GRAD_APPLICANT_GROUP);
        }

        if (GRAD_ADMITTED_GROUP_EXPRESSION) {
            user.getGroups().add(GRAD_ADMITTED_GROUP);
        }
        if (GRAD_DEPOSITED_GROUP_EXPRESSION) {
            user.getGroups().add(GRAD_DEPOSITED_GROUP);
        }
        if (GRAD_DEPOSITED_AND_ADMITTED_GROUP_EXPRESSION) {
            user.getGroups().add(GRAD_DEPOSITED_AND_ADMITTED_GROUP);
        }
        if (UG_APPLICANT_GROUP_EXPRESSION) {
            user.getGroups().add(UG_APPLICANT_GROUP);
        }
        if (UG_ADMITTED_GROUP_EXPRESSION) {
            user.getGroups().add(UG_ADMITTED_GROUP);
        }
        if (UG_DEPOSITED_GROUP_EXPRESSION) {
            user.getGroups().add(UG_DEPOSITED_GROUP);
        }
        if (UG_DEPOSITED_AND_ADMITTED_GROUP_EXPRESSION) {
            user.getGroups().add(UG_DEPOSITED_AND_ADMITTED_GROUP);
        }
        if (UG_NEWSTUDENT_GROUP_EXPRESSION) {
            user.getGroups().add(UG_NEWSTUDENT_GROUP);
        }
        if (ALL_ADMITTED_GROUP_EXPRESSION) {
            user.getGroups().add(ALL_ADMITTED_GROUP);
        }

        /*###############################################################################
            Adding Groups to User's Object w.r.t Tabs.
          ###############################################################################*/

        /*Add Torero Hub Tab-> Group if Applicable.
         * Pages (Under Torero Hub Tab) that have same access as Torero Hub Tab:
         * 1. FERPA
         * 2. Analytics
         * 3. My Torero Services
         * */
        if (TOREROHUB_TAB_GROUP_EXPRESSION) {
            user.getGroups().add(TOREROHUB_TAB_GROUP);
        }
        /*Add Torero Life Tab-> Group if Applicable.*/
        if (TORERO_LIFE_TAB_GROUP_EXPRESSION) {
            user.getGroups().add(TORERO_LIFE_TAB_GROUP);
        }
        /*Add Graduate Admissions Tab-> Group if Applicable.*/
        if (GRAD_ADMISSIONS_TAB_GROUP_EXPRESSION) {
            user.getGroups().add(GRAD_ADMISSIONS_TAB_GROUP);
        }

        /*Add Undergrad Admissions Tab-> Group if Applicable.*/
        if (UG_ADMISSIONS_TAB_GROUP_EXPRESSION) {
            user.getGroups().add(UG_ADMISSIONS_TAB_GROUP);
        }

        /*Add FST Student Tab Group-> if Applicable.*/
        if (FST_STUDENT_TAB_GROUP_EXPRESSIONS) {
            user.getGroups().add(FST_STUDENT_TAB_GROUP);
        }
        /*Add Employee Tab Group if-> applicable.*/
        if (EMPLOYEE_TAB_GROUP_EXPRESSION) {
            user.getGroups().add(EMPLOYEE_TAB_GROUP);
        }
        /*Add Help Desk & Support Tab-> Group if applicable.*/
        if (HELP_DESK_AND_SUPPORT_TAB_GROUP_EXPRESSION) {
            user.getGroups().add(HELP_DESK_AND_SUPPORT_TAB_GROUP);
        }

        /*Add Torero Alumni Tab Group-> if applicable.*/
        if (ALUMNI_TAB_GROUP_EXPRESSION) {
            user.getGroups().add(ALUMNI_TAB_GROUP);
        }
        /*Add Faculty and Dept Chairs Tab-> Group if applicable.*/
        if (FACULTY_AND_DEPT_CHAIRS_TAB_GROUP_EXPRESSION) {
            user.getGroups().add(FACULTY_AND_DEPT_CHAIRS_TAB_GROUP);
        }

        /*Add Law Admission Tab-> Group if Applicable.*/
        if (LAW_ADMISSIONS_TAB_GROUP_EXPRESSION) {
            user.getGroups().add(LAW_ADMISSIONS_TAB_GROUP);
        }

        /*Add Application And Tools Tab-> Group if Applicable.*/
        if (APPLICATIONS_AND_TOOLS_TAB_GROUP_EXPRESSION) {
            user.getGroups().add(APPLICATIONS_AND_TOOLS_TAB_GROUP);
        }
        /*Add Application And Tools Tab-> Group if Applicable.*/
        if (ALL_ACTIVE_STUDENTS_AND_ALL_APPLICANTS_GROUP_EXPRESSION) {
            user.getGroups().add(ALL_ACTIVE_STUDENTS_AND_ALL_APPLICANTS_GROUP);
        }


        /*###############################################################################
            Adding Groups to User's Object w.r.t Pages under Tabs.
          ###############################################################################*/

        /* Add Student Employment Page Group under Torero Hub Tab-> */
        if (STUDENT_EMPLOYMENT_PAGE_GROUP_EXPRESSION) {
            user.getGroups().add(STUDENT_EMPLOYMENT_PAGE_GROUP);
        }

        /* Add Student Employment Page Group under Torero Hub Tab-> */
        if (STUDENT_EMPLOYMENT_PAGE_GROUP_EXPRESSION) {
            user.getGroups().add(STUDENT_EMPLOYMENT_PAGE_GROUP);
        }
        if (USDONE_PAGE_GROUP_EXPRESSION) {
            user.getGroups().add(USDONE_PAGE_GROUP);
        }

        if (MY_SUPPORT_SERVICES_PAGE_GROUP_EXPRESSION) {
            user.getGroups().add(MY_SUPPORT_SERVICES_PAGE_GROUP);
        }

        if (SAFETY_CHECK_PAGE_GROUP_EXPRESSION) {
            user.getGroups().add(SAFETY_CHECK_PAGE_GROUP);
        }
        if (EVENTS_PAGE_GROUP_EXPRESSION) {
            user.getGroups().add(EVENTS_PAGE_GROUP);
        }

        return personDetailedInfo;
    }

}