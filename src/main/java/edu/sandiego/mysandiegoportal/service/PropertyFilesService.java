package edu.sandiego.mysandiegoportal.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import java.io.*;
import java.util.Properties;
import static edu.sandiego.mysandiegoportal.domain.ApplicationFinals.MYSANDIEGOPORTAL_CONFIG_FILE_FULL_PATH;

/**
 * Created by rdhiman on Mar.23.2021.
 */
@Service("PropertyFilesService")
public class PropertyFilesService {

    private static Logger log = LogManager.getLogger(PropertyFilesService.class.getName());

    /**
     * Returns a property for a given propertyName.
     *
     * @param propertyName One of the properties from the property file.
     * @return The value of the property, or null if not found.
     */
    public String getPropertyValue(final String propertyName) {
        Properties prop = new Properties();
        InputStream input;
        try {
            input = new FileInputStream(MYSANDIEGOPORTAL_CONFIG_FILE_FULL_PATH);
            prop.load(input);
            input.close();
            return prop.getProperty(propertyName);
        } catch (FileNotFoundException e) {
            log.error("Could not find property file: " + e.getMessage());
        } catch (IOException e) {
            log.error("IOException: " + e.getMessage());
        }
        return null;
    }
}
