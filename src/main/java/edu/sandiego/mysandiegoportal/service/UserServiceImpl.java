package edu.sandiego.mysandiegoportal.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import edu.sandiego.mysandiegoportal.domain.MySanDiegoPortalFinals;
import edu.sandiego.mysandiegoportal.domain.PersonDetailedInfo;
import edu.sandiego.mysandiegoportal.domain.User;
import org.apache.logging.log4j.LogManager;
import org.json.simple.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.net.ssl.SSLContext;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by rdhiman on Feb.23.2021.
 */
@Service
public class UserServiceImpl implements UserService {

    /**
     * Logger.
     */
    private static org.apache.logging.log4j.Logger log = LogManager.getLogger(UserServiceImpl.class.getName());
    private static final Logger LOG = LoggerFactory.getLogger(UserServiceImpl.class);
    private static final List<User> USERS = new ArrayList<>();

    public String getToken() {
        TokenService tokenService = new TokenService();
        String token = tokenService.getUsdSecurityToken();
        return token;
    }

    @Override
    public PersonDetailedInfo getUserByUsername(String username) {
        JSONArray jsonArray = new JSONArray();
        String token = getToken();
        assert token != null : "Token was null.";
        Response response = null;
        ObjectMapper mapper = new ObjectMapper();

        try {
            //Converting the Object to JSONString
            //String jsonString = mapper.writeValueAsString(usdOneUserInfo);
            SSLContext context = SSLContext.getInstance("TLSv1.2");
            context.init(null, null, null);
            Client client =
                    ClientBuilder.newBuilder().sslContext(context).build();
            PersonDetailedInfo user = client
                    .target(MySanDiegoPortalFinals.USDSECURITY_V_1_PERSON)
                    .path(MySanDiegoPortalFinals.BYUSERNAME)
                    .queryParam("username",username)
                    .request(MediaType.APPLICATION_JSON_TYPE)
                    .header("Authorization", "Bearer " + token)
                    .get(PersonDetailedInfo.class);
            return user;
        } catch (NoSuchAlgorithmException e) {

            log.error("NoSuchAlgorithmException: " + e.getMessage());
        } catch (KeyManagementException e) {

            log.error("KeyManagementException: " + e.getMessage());
        } catch (ClassCastException e2) {

            log.error("Received class cast exception. Likely cause is that the user passed an invalid Salesforce token:"
                    + e2.getMessage());
        } catch (Exception e1) {

            log.error("Unknown exception: " + e1.getMessage());
        }
        return null;
    }
}

