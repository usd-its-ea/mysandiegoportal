package edu.sandiego.mysandiegoportal;


import org.jasig.cas.client.boot.configuration.EnableCasClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

/**
 * Created by rdhiman on Feb.21.2021.
 */
@Configuration
@EnableWebSecurity
@EnableCasClient
public class CASWebSecurityConfig extends WebSecurityConfigurerAdapter {
    private Logger logger = LoggerFactory.getLogger(CASWebSecurityConfig.class);

    @Autowired
    PostAuthenticationSuccessHandler postAuthenticationSuccessHandler;

    @Value("${casLogoutUrl}")
    private String casLogoutUrl;

    /*@Override
    public void configure(WebSecurity web) {
        web.ignoring().antMatchers("/resources/**");
    }*/

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/**").permitAll()
                .anyRequest().authenticated()
                .and()
                .logout().logoutSuccessUrl(casLogoutUrl);
                //.and().addFilterAfter(new CustomFilter(), BasicAuthenticationFilter.class);

        http.csrf().disable();
    }
    /*@Bean
    public AuthenticationUserDetailsService<CasAssertionAuthenticationToken> springSecurityCasUserDetailsService() {
        return null;
    }*/

    /*private CasAuthenticationProvider casAuthenticationProvider;
    private ServiceProperties serviceProperties;
    private SingleSignOutFilter singleSignOutFilter;
    private LogoutFilter logoutFilter;

    *//*@Autowired
    public CASWebSecurityConfig(SingleSignOutFilter singleSignOutFilter, LogoutFilter logoutFilter, CasAuthenticationProvider casAuthenticationProvider, ServiceProperties serviceProperties) {
        this.logoutFilter = logoutFilter;
        this.singleSignOutFilter = singleSignOutFilter;
        this.serviceProperties = serviceProperties;
        this.casAuthenticationProvider = casAuthenticationProvider;
    }*//*

     *//*   @Autowired
    UserPrincipalDetailsService userPrincipalDetailsService;

    public CASWebSecurityConfig(UserPrincipalDetailsService userPrincipalDetailsService) {
        this.userPrincipalDetailsService = userPrincipalDetailsService;
    }*//*
     *//* @Autowired
    DataSource dataSource;*//*

    @Autowired
    PostAuthenticationSuccessHandler postAuthenticationSuccessHandler;
    *//* This configure method is overridden for Authentication purposes.*//*
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(casAuthenticationProvider);
        *//*auth.jdbcAuthentication()
                .dataSource(dataSource)
                .usersByUsernameQuery("select GORPAUD_EXTERNAL_USER as username, GORPAUD_PIN as password, 'TRUE' " +
                        "from GORPAUD " +
                        "where GORPAUD_EXTERNAL_USER = ?")
                .authoritiesByUsernameQuery("select gobtpac_external_user as username, gorirol_role as authority  " +
                        "from gorirol, gobtpac " +
                        "where gorirol_pidm = gobtpac_pidm " +
                        "and gobtpac_external_user = ?");*//*
    }

    *//* This configure method is overridden for Authorization purposes.*//*
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                // Admin Matchers
                .antMatchers("/")
                .authenticated()
                .and().exceptionHandling()
                .authenticationEntryPoint(authenticationEntryPoint());
                //.and()
                //.addFilterBefore(singleSignOutFilter, CasAuthenticationFilter.class)
                //.addFilterBefore(logoutFilter, LogoutFilter.class)
//                .antMatchers("/admin").hasRole("ADMIN")
//                // Admin Matchers
//                .antMatchers("/user").hasAnyRole("USER", "ADMIN")
//                // Admin Matchers
//                //TODO: find out if hasAnyRole works the same way as hasAnyAuthority
//                .antMatchers("/finaid").hasAnyAuthority("ALL_FALL", "APPLICANT")
//                // Admin Matchers
//                .antMatchers("/").permitAll()
                //.exceptionHandling().accessDeniedPage("/errors/403")
                //Form Login Configuration
                //.and().formLogin()
                //.successHandler(postAuthenticationSuccessHandler);
                *//*.defaultSuccessUrl("/default")
                .permitAll();*//*
     *//*  // Logout Configuration
                // CSRF is enabled by default, with Java Config
                .and().csrf().disable()
                // Cors Origin Resource Sharing
                .cors().disable()
                // HTTP Security Headers
                .headers().disable();*//*
    }

    @Override
    public void configure(final WebSecurity web) {
        //We don't need to secure these files.
        web.ignoring()
                .antMatchers("/css/**")
                .antMatchers("/img/**")
                .antMatchers("/web/jars/**");
    }

    @Bean
    public PasswordEncoder getPasswordEncoder() {
        return NoOpPasswordEncoder.getInstance();
    }

    *//* CAS Beans Config *//*

    @Bean
    @Override
    protected AuthenticationManager authenticationManager() throws Exception {
        return new ProviderManager(Collections.singletonList(casAuthenticationProvider));
    }
    @Bean
    public CasAuthenticationEntryPoint authenticationEntryPoint() {
        CasAuthenticationEntryPoint entryPoint = new CasAuthenticationEntryPoint();
        entryPoint.setLoginUrl("http://localhost:8080/cas/login");
        entryPoint.setServiceProperties(serviceProperties);
        return entryPoint;
    }*/


}
