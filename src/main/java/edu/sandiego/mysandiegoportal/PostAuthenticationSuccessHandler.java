package edu.sandiego.mysandiegoportal;

import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.*;

/**
 * Created by rdhiman on Feb.22.2021.
 */
@Component
public class PostAuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {
    //public class PostAuthenticationSuccessHandler extends AuthenticationSuccessHandler {

    protected RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        handle(request, response, authentication);
        clearAuthenticationAttributes(request);
    }
    protected void handle(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException {

        String targetUrl = determineTargetUrl(authentication);
        if (response.isCommitted()) {
            System.out.println("Response has already been committed. Unable to redirect to " + targetUrl);
            return;
        }
        redirectStrategy.sendRedirect(request, response, targetUrl);
    }

    protected String determineTargetUrl(final Authentication authentication) {

        System.out.println("....Post Authentication Method is called...");
        UserDetails details = (UserDetails) authentication.getPrincipal();
        System.out.println("#########################################");
        System.out.println(details.getUsername() +" is logged in");
        System.out.println("#########################################");
        /*Collection<GrantedAuthority> authorities = (Collection<GrantedAuthority>) authentication.getAuthorities();
        List<GrantedAuthority> listAuthorities = new ArrayList<GrantedAuthority>();
        listAuthorities.addAll(authorities);
        for(GrantedAuthority authority: listAuthorities) {
            if(authority.getAuthority().equals("ALL_FALL") || authority.getAuthority().equals("APPLICANT")) {
                return ("/applicant");
            }
        }*/
        throw new IllegalStateException();
    }


  /*  @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
                                        Authentication authentication) throws IOException, ServletException {
        System.out.println("....Post Authentication Method is called...");
        UserDetails details = (UserDetails) authentication.getPrincipal();
        System.out.println("#########################################");
        System.out.println(details.getUsername() +" is logged in");
        System.out.println("#########################################");
        Collection<GrantedAuthority> authorities = (Collection<GrantedAuthority>) authentication.getAuthorities();
        List<GrantedAuthority> listAuthorities = new ArrayList<GrantedAuthority>();
        listAuthorities.addAll(authorities);
        for(GrantedAuthority authority: listAuthorities) {
            if(authority.getAuthority().equals("ALL_FALL") || authority.getAuthority().equals("APPLICANT")) {
                redirectStrategy.sendRedirect(request, response,"/applicant");
            }
        }
    }*/

}
