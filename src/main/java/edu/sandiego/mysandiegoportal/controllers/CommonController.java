package edu.sandiego.mysandiegoportal.controllers;

import edu.sandiego.mysandiegoportal.controllers.student.ELAController;
import edu.sandiego.mysandiegoportal.domain.PersonDetailedInfo;
import edu.sandiego.mysandiegoportal.service.GroupAdminService;
import edu.sandiego.mysandiegoportal.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by rdhiman on Mar.26.2021.
 */
@Controller
@RequestMapping("/common")
public class CommonController {

    private static Logger log = LogManager.getLogger(CommonController.class.getName());
    @Autowired
    UserService userService;
    @Autowired
    GroupAdminService groupAdminService;

    @GetMapping("myacademics")
    public String myacademics(Model model, HttpServletRequest request) {

        String currentPrincipalName = request.getUserPrincipal().toString();
        try {
            PersonDetailedInfo personDetailedInfo = userService.getUserByUsername(currentPrincipalName);
            personDetailedInfo = groupAdminService.setMyGroups(personDetailedInfo);
            model.addAttribute("personDetailedInfo", personDetailedInfo);
            log.info(">>>>>>>>>>>>> In CommonController:-> myacademics " + personDetailedInfo.getBasicPersonInfo().getFirstName());
            return "common-pages/myacademics";
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Unknown exception: " + e.getMessage());
            return "error";
        }
    }

    @GetMapping("studentemployment")
    public String studentemployment(Model model, HttpServletRequest request) {

        String currentPrincipalName = request.getUserPrincipal().toString();
        try {
            PersonDetailedInfo personDetailedInfo = userService.getUserByUsername(currentPrincipalName);
            personDetailedInfo = groupAdminService.setMyGroups(personDetailedInfo);
            model.addAttribute("personDetailedInfo", personDetailedInfo);
            log.info(">>>>>>>>>>>>> In CommonController:-> studentemployment " + personDetailedInfo.getBasicPersonInfo().getFirstName());
            return "common-pages/studentemployment";
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Unknown exception: " + e.getMessage());
            return "error";
        }
    }

    @GetMapping("mystudentaccount")
    public String mystudentaccounts(Model model, HttpServletRequest request) {

        String currentPrincipalName = request.getUserPrincipal().toString();
        try {
            PersonDetailedInfo personDetailedInfo = userService.getUserByUsername(currentPrincipalName);
            personDetailedInfo = groupAdminService.setMyGroups(personDetailedInfo);
            model.addAttribute("personDetailedInfo", personDetailedInfo);
            log.info(">>>>>>>>>>>>> In CommonController:-> mystudentaccounts " + personDetailedInfo.getBasicPersonInfo().getFirstName());
            return "students/common-student-pages/mystudentaccount";
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Unknown exception: " + e.getMessage());
            return "error";
        }
    }

    @GetMapping("financialaid")
    public String financialAid(Model model, HttpServletRequest request) {

        String currentPrincipalName = request.getUserPrincipal().toString();
        try {
            PersonDetailedInfo personDetailedInfo = userService.getUserByUsername(currentPrincipalName);
            personDetailedInfo = groupAdminService.setMyGroups(personDetailedInfo);
            model.addAttribute("personDetailedInfo", personDetailedInfo);
            log.info(">>>>>>>>>>>>> In CommonController:-> financialAid " + personDetailedInfo.getBasicPersonInfo().getFirstName());
            return "students/common-student-pages/financialaid";
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Unknown exception: " + e.getMessage());
            return "error";
        }
    }
}
