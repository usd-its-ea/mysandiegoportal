package edu.sandiego.mysandiegoportal.controllers;

import edu.sandiego.mysandiegoportal.domain.PersonDetailedInfo;
import edu.sandiego.mysandiegoportal.service.GroupAdminService;
import edu.sandiego.mysandiegoportal.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jasig.cas.client.authentication.AttributePrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.server.Session;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import static edu.sandiego.mysandiegoportal.domain.GroupNames.*;


/**
 * Created by rdhiman on Feb.21.2021.
 */
@Controller
public class WelcomeController {

    private static Logger log = LogManager.getLogger(WelcomeController.class.getName());
    @Autowired
    UserService userService;
    @Autowired
    GroupAdminService groupAdminService;

    @GetMapping({"/", "welcome"})
    public String index(Model model, HttpServletRequest request) {
        HttpSession session = request.getSession();
        if (session == null) {
            return "error";
        } else {
            String currentPrincipalName = request.getUserPrincipal().toString();
            try {
                PersonDetailedInfo personDetailedInfo = userService.getUserByUsername(currentPrincipalName);
                personDetailedInfo = groupAdminService.setMyGroups(personDetailedInfo);
                session.setAttribute("personDetailedInfo", personDetailedInfo);
                model.addAttribute("personDetailedInfo", personDetailedInfo);
                log.info(">>>>>>>>>>>>> User info. for " + currentPrincipalName + " : " + personDetailedInfo.getBasicPersonInfo().getFirstName());

                if (personDetailedInfo.getBasicPersonInfo().getGroups().contains(PORTAL_ADMIN_GROUP)) {
                    //return ug announcement page
                    return "welcome";
                } else if (personDetailedInfo.getBasicPersonInfo().getGroups().contains(UG_ADMISSIONS_TAB_GROUP)) {
                    //return ug announcement page
                    return "students/undergrad/undergrad-home";
                } else if (personDetailedInfo.getBasicPersonInfo().getGroups().contains(GRAD_ADMISSIONS_TAB_GROUP)) {
                    // return grad announcement page
                    return "students/grad/grad-home";
                } else if (personDetailedInfo.getBasicPersonInfo().getGroups().contains(EMPLOYEE_GROUP)) {
                    // return employee welcome page
                    return "employee/home";
                }
                return "welcome";
            } catch (Exception e) {
                e.printStackTrace();
                log.error("Unknown exception: " + e.getMessage());
                return "error";
            }
        }
    }

    @GetMapping("/admin")
    public String admin() {
        return "view/admin";
    }

    @GetMapping("/errors/403")
    public String forbidden() {
        return "errors/403";
    }
}
