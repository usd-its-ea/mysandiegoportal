package edu.sandiego.mysandiegoportal.controllers.employee;

import edu.sandiego.mysandiegoportal.service.TokenService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.net.ssl.SSLContext;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

import static edu.sandiego.mysandiegoportal.domain.MySanDiegoPortalFinals.USD_SECURITY_BASE_URL;
import static edu.sandiego.mysandiegoportal.domain.MySanDiegoPortalFinals.USD_SECURITY_FAVORITE_TOOLS_PATH;

/**
 * Created by rdhiman on Mar.23.2021.
 */
@RestController
@RequestMapping("rest/employee")
public class EmployeeRestController {

    private static Logger log = LogManager.getLogger(EmployeeRestController.class.getName());

    public String getToken() {
        TokenService tokenService = new TokenService();
        String token = tokenService.getUsdSecurityToken();
        return token;
    }

    @GetMapping("saveMyFavoritesApplicationsPreferences")
    public final String saveMyFavoritesApplicationsPreferences(
            @RequestParam(value = "username", required = false) final String username,
            @RequestParam(value = "appId", required = false) final String appId,
            @RequestParam(value = "appName", required = false) final String appName,
            @RequestParam(value = "appDescription", required = false) final String appDescription,
            @RequestParam(value = "appURL", required = false) final String appURL,
            @RequestParam(value = "appCategory", required = false) final String appCategory,
            @RequestParam(value = "isFavorite", required = false) final String isFavorite,
            @RequestParam(value = "targetAction", required = false) final String targetAction) {

        String token = getToken();
        assert token != null : "Token was null.";

        try {
            Form form = new Form();
            form.param("environmentType", "PROD");
            form.param("username", username);
            form.param("appId", appId);
            form.param("appName", appName);
            form.param("appDescription", appDescription);
            form.param("appURL", appURL);
            form.param("appCategory", appCategory);
            form.param("isFavorite", isFavorite);

            Response responseEntity = null;
            SSLContext context = SSLContext.getInstance("TLSv1.2");
            context.init(null, null, null);
            Client client =
                    ClientBuilder.newBuilder().sslContext(context).build();
            if (targetAction.equalsIgnoreCase("save")) {
                responseEntity =
                        client
                                .target(USD_SECURITY_BASE_URL)
                                .path(USD_SECURITY_FAVORITE_TOOLS_PATH)
                                .request(MediaType.APPLICATION_JSON)
                                .header("Authorization", "Bearer " + token)
                                .post(Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED_TYPE), Response.class);

            } else if (targetAction.equalsIgnoreCase("delete")) {
                responseEntity =
                        client
                                .target(USD_SECURITY_BASE_URL)
                                .path(USD_SECURITY_FAVORITE_TOOLS_PATH)
                                .queryParam("environmentType", "PROD")
                                .queryParam("username", username)
                                .queryParam("appId", appId)
                                .queryParam("isFavorite", isFavorite)
                                .request(MediaType.APPLICATION_JSON)
                                .header("Authorization", "Bearer " + token)
                                .delete();
            }
            String stringResponse = responseEntity.readEntity(String.class);
            return stringResponse;
        } catch (NoSuchAlgorithmException e) {

            log.error("NoSuchAlgorithmException: " + e.getMessage());
        } catch (KeyManagementException e) {

            log.error("KeyManagementException: " + e.getMessage());
        } catch (ClassCastException e2) {

            log.error("Received class cast exception. Likely cause is that the user passed an invalid Salesforce token:"
                    + e2.getMessage());
        } catch (Exception e1) {

            log.error("Unknown exception: " + e1.getMessage());
        }
        return null;
    }
}
