package edu.sandiego.mysandiegoportal.controllers.employee;

import edu.sandiego.mysandiegoportal.domain.PersonDetailedInfo;
import edu.sandiego.mysandiegoportal.service.AuthorizationService;
import edu.sandiego.mysandiegoportal.service.GroupAdminService;
import edu.sandiego.mysandiegoportal.service.UserService;
import edu.sandiego.mysandiegoportal.service.employee.EmployeeService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import static edu.sandiego.mysandiegoportal.domain.URI.EMPLOYEE_APPLICATIONS_AND_TOOLS_URI;

/**
 * Created by rdhiman on Mar.22.2021.
 */
@Controller
@RequestMapping("/employee")
public class EmployeeController {

    private static Logger log = LogManager.getLogger(EmployeeController.class.getName());
    /*TODO: Move userService and groupAdminService related operations to Post Login Success Handler. So that it is
     *  called at the entry point and saved into session/model. No need to call on every Controller.*/
    @Autowired
    UserService userService;
    @Autowired
    GroupAdminService groupAdminService;
    @Autowired
    EmployeeService employeeService;
    @Autowired
    AuthorizationService authorizationService;

    @GetMapping("**")
    public String employee(Model model, HttpServletRequest request) {
        HttpSession session = request.getSession();
        String requestURI = request.getRequestURI();
        String currentPrincipalName = request.getUserPrincipal().toString();

        if (session == null) {
            return "error";
        } else {
            try {
                if (session.getAttribute("personDetailedInfo") == null) {
                    PersonDetailedInfo personDetailedInfo = userService.getUserByUsername(currentPrincipalName);
                    personDetailedInfo = groupAdminService.setMyGroups(personDetailedInfo);
                    /* Making sure user has right permissions to access Employee related stuff. (Tabs + Pages). */
                    if (authorizationService.isUserAuthorized(personDetailedInfo, requestURI)) {
                        model.addAttribute("personDetailedInfo", personDetailedInfo);
                        session.setAttribute("personDetailedInfo", personDetailedInfo);
                        /* If users is accessing Employee -> Applications And Tools view: do the following. */
                        if (getViewName(getViewName(requestURI)).equalsIgnoreCase(EMPLOYEE_APPLICATIONS_AND_TOOLS_URI)) {
                            /* Add following json file only if user is accessing applicationsAndTools view. */
                            JSONArray allApplicationAndToolsJsonArray = employeeService.loadApplicationsAndToolsFromJSONFile();
                            JSONArray myFavoriteApplicationsAndToolsJsonArray = employeeService.getMyFavAppsIdFromBanner(personDetailedInfo.getBasicPersonInfo().getUsername());
                            model.addAttribute("myFavoriteApplicationsAndToolsJsonArray", myFavoriteApplicationsAndToolsJsonArray);
                            model.addAttribute("allApplicationAndToolsJsonArray", allApplicationAndToolsJsonArray);
                        }
                        return getViewName(requestURI);
                    } else {
                        model.addAttribute("personDetailedInfo", personDetailedInfo);
                        session.setAttribute("personDetailedInfo", personDetailedInfo);
                        return "404";
                    }
                } else {
                    PersonDetailedInfo personDetailedInfo = (PersonDetailedInfo) session.getAttribute("personDetailedInfo");
                    if (authorizationService.isUserAuthorized(personDetailedInfo, requestURI)) {
                        model.addAttribute("personDetailedInfo", personDetailedInfo);
                        if (getViewName(getViewName(requestURI)).equalsIgnoreCase(EMPLOYEE_APPLICATIONS_AND_TOOLS_URI)) {
                            /* Add following json file only if user is accessing applicationsAndTools view. */
                            JSONArray allApplicationAndToolsJsonArray = employeeService.loadApplicationsAndToolsFromJSONFile();
                            JSONArray myFavoriteApplicationsAndToolsJsonArray = employeeService.getMyFavAppsIdFromBanner(personDetailedInfo.getBasicPersonInfo().getUsername());
                            model.addAttribute("myFavoriteApplicationsAndToolsJsonArray", myFavoriteApplicationsAndToolsJsonArray);
                            model.addAttribute("allApplicationAndToolsJsonArray", allApplicationAndToolsJsonArray);
                        }
                        return getViewName(requestURI);
                    } else {
                        model.addAttribute("personDetailedInfo", personDetailedInfo);
                        return "404";
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                log.error("Unknown exception: " + e.getMessage());
                return "error";
            }
        }
    }

    private String getViewName(String requestURI) {

        if (requestURI.contains(EMPLOYEE_APPLICATIONS_AND_TOOLS_URI)) {
            return "employee/applicationsAndTools";
        } else if (requestURI.contains("teach")) {
            return "employee/teach";
        } else if (requestURI.contains("advise")) {
            return "employee/advise";
        } else if (requestURI.contains("home")
                || requestURI.endsWith("employee/")) {
            return "employee/home";
        }
        return null;
    }

}
