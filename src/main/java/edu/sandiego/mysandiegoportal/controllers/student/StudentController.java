package edu.sandiego.mysandiegoportal.controllers.student;

import edu.sandiego.mysandiegoportal.domain.PersonDetailedInfo;
import edu.sandiego.mysandiegoportal.service.GroupAdminService;
import edu.sandiego.mysandiegoportal.service.UserService;
import edu.sandiego.mysandiegoportal.service.student.StudentService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by rdhiman on Mar.03.2021.
 */
@Controller
@RequestMapping("/student")
public class StudentController {

    private static Logger log = LogManager.getLogger(StudentController.class.getName());
    @Autowired
    UserService userService;
    @Autowired
    GroupAdminService groupAdminService;
    @Autowired
    StudentService studentService;


    @GetMapping("studentemployment")
    public String studentemployment(Model model, HttpServletRequest request) {

        String currentPrincipalName = request.getUserPrincipal().toString();
        try {
            PersonDetailedInfo personDetailedInfo = userService.getUserByUsername(currentPrincipalName);
            personDetailedInfo = groupAdminService.setMyGroups(personDetailedInfo);
            model.addAttribute("personDetailedInfo", personDetailedInfo);
            log.info(">>>>>>>>>>>>> In StudentController:-> studentemployment " + personDetailedInfo.getBasicPersonInfo().getFirstName());
            return "students/studentemployment";
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Unknown exception: " + e.getMessage());
            return "error";
        }
    }

    @GetMapping("applicationsAndTools")
    public String applicationsAndTools(Model model, HttpServletRequest request) {

        String currentPrincipalName = request.getUserPrincipal().toString();
        try {
            PersonDetailedInfo personDetailedInfo = userService.getUserByUsername(currentPrincipalName);
            personDetailedInfo = groupAdminService.setMyGroups(personDetailedInfo);
            model.addAttribute("personDetailedInfo", personDetailedInfo);
            log.info(">>>>>>>>>>>>> User info. for " + currentPrincipalName + " : " + personDetailedInfo.getBasicPersonInfo().getFirstName());
            JSONArray allApplicationAndToolsJsonArray = studentService.loadApplicationsAndToolsFromJSONFile();
            JSONArray myFavoriteApplicationsAndToolsJsonArray = studentService.getMyFavAppsIdFromBanner(personDetailedInfo.getBasicPersonInfo().getUsername());
            model.addAttribute("myFavoriteApplicationsAndToolsJsonArray", myFavoriteApplicationsAndToolsJsonArray);
            model.addAttribute("allApplicationAndToolsJsonArray", allApplicationAndToolsJsonArray);
            return "students/applicationsAndTools";
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Unknown exception: " + e.getMessage());
            return "error";
        }
    }
}
