package edu.sandiego.mysandiegoportal.controllers.student;

import edu.sandiego.mysandiegoportal.controllers.WelcomeController;
import edu.sandiego.mysandiegoportal.domain.PersonDetailedInfo;
import edu.sandiego.mysandiegoportal.service.AuthorizationService;
import edu.sandiego.mysandiegoportal.service.GroupAdminService;
import edu.sandiego.mysandiegoportal.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Created by rdhiman on Mar.25.2021.
 */
@Controller
@RequestMapping("/student/law")
public class LawController {

    private static Logger log = LogManager.getLogger(LawController.class.getName());

    @Autowired
    UserService userService;
    @Autowired
    GroupAdminService groupAdminService;
    @Autowired
    AuthorizationService authorizationService;

    @GetMapping("**")
    public String undergradStudents(Model model, HttpServletRequest request) {
        HttpSession session = request.getSession();
        String requestURI = request.getRequestURI();
        String currentPrincipalName = request.getUserPrincipal().toString();
        if (session == null) {
            return "error";
        } else {
            try {
                if (session.getAttribute("personDetailedInfo") == null) {
                    PersonDetailedInfo personDetailedInfo = userService.getUserByUsername(currentPrincipalName);
                    personDetailedInfo = groupAdminService.setMyGroups(personDetailedInfo);
                    /* Making sure user has right permissions to access Undergrad Student related stuff. (Tabs + Pages). */
                    if (authorizationService.isUserAuthorized(personDetailedInfo, requestURI)) {
                        model.addAttribute("personDetailedInfo", personDetailedInfo);
                        session.setAttribute("personDetailedInfo", personDetailedInfo);
                        /* If users is accessing Employee -> Applications And Tools view: do the following. */
                        return getViewName(requestURI);
                    } else {
                        model.addAttribute("personDetailedInfo", personDetailedInfo);
                        session.setAttribute("personDetailedInfo", personDetailedInfo);
                        return "404";
                    }
                } else {
                    PersonDetailedInfo personDetailedInfo = (PersonDetailedInfo) session.getAttribute("personDetailedInfo");
                    if (authorizationService.isUserAuthorized(personDetailedInfo, requestURI)) {
                        model.addAttribute("personDetailedInfo", personDetailedInfo);
                        return getViewName(requestURI);
                    } else {
                        model.addAttribute("personDetailedInfo", personDetailedInfo);
                        return "404";
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                log.error("Unknown exception: " + e.getMessage());
                return "error";
            }
        }
    }

   /* @GetMapping("admissions")
    public String lawAdmissions(Model model, HttpServletRequest request) {

        String currentPrincipalName = request.getUserPrincipal().toString();
        try {
            PersonDetailedInfo personDetailedInfo = userService.getUserByUsername(currentPrincipalName);
            personDetailedInfo = groupAdminService.setMyGroups(personDetailedInfo);
            model.addAttribute("personDetailedInfo", personDetailedInfo);
            log.info(">>>>>>>>>>>>> In LawController:-> lawAdmissions " + personDetailedInfo.getBasicPersonInfo().getFirstName());
            return "students/law/law-admissions";
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Unknown exception: " + e.getMessage());
            return "error";
        }
    }

    @GetMapping("student")
    public String lawStudent(Model model, HttpServletRequest request) {

        String currentPrincipalName = request.getUserPrincipal().toString();
        try {
            PersonDetailedInfo personDetailedInfo = userService.getUserByUsername(currentPrincipalName);
            personDetailedInfo = groupAdminService.setMyGroups(personDetailedInfo);
            model.addAttribute("personDetailedInfo", personDetailedInfo);
            log.info(">>>>>>>>>>>>> In LawController:-> lawStudent " + personDetailedInfo.getBasicPersonInfo().getFirstName());
            return "students/law/lawstudent";
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Unknown exception: " + e.getMessage());
            return "error";
        }
    }

    @GetMapping("paralegal")
    public String paralegal(Model model, HttpServletRequest request) {

        String currentPrincipalName = request.getUserPrincipal().toString();
        try {
            PersonDetailedInfo personDetailedInfo = userService.getUserByUsername(currentPrincipalName);
            personDetailedInfo = groupAdminService.setMyGroups(personDetailedInfo);
            model.addAttribute("personDetailedInfo", personDetailedInfo);
            log.info(">>>>>>>>>>>>> In LawController:-> paralegal " + personDetailedInfo.getBasicPersonInfo().getFirstName());
            return "students/law/paralegal";
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Unknown exception: " + e.getMessage());
            return "error";
        }
    }*/

    private String getViewName(String requestURI) {

        if (requestURI.contains("/mysandiegoportal/student/law/admissions")) {
            return "students/law/admissions";
        } else if (requestURI.contains("/mysandiegoportal/student/law/student")) {
            return "students/law/student";
        } else if (requestURI.contains("/mysandiegoportal/student/law/paralegal")) {
            return "students/law/paralegal";
        }
        return null;
    }
}
