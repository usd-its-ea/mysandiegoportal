package edu.sandiego.mysandiegoportal;

import edu.sandiego.mysandiegoportal.domain.PersonDetailedInfo;
import edu.sandiego.mysandiegoportal.service.GroupAdminService;
import edu.sandiego.mysandiegoportal.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.ui.Model;
import org.springframework.web.filter.GenericFilterBean;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by rdhiman on Mar.31.2021.
 */
public class CustomFilter extends UsernamePasswordAuthenticationFilter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        //System.out.println("##############################################################");
        //System.out.println("Good fit for authorization?");
        //System.out.println("##############################################################");
        //chain.doFilter(request, response);
    }
}
