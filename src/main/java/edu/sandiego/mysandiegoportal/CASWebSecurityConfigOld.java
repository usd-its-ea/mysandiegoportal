package edu.sandiego.mysandiegoportal;


//import org.jasig.cas.client.boot.configuration.EnableCasClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.ProviderManager;
//import org.springframework.security.cas.ServiceProperties;
//import org.springframework.security.cas.authentication.CasAuthenticationProvider;
//import org.springframework.security.cas.web.CasAuthenticationEntryPoint;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import java.util.Collections;

/**
 * Created by rdhiman on Feb.21.2021.
 */
//@Configuration
//@EnableWebSecurity
//@EnableCasClient
public class CASWebSecurityConfigOld extends WebSecurityConfigurerAdapter {

    private Logger logger = LoggerFactory.getLogger(CASWebSecurityConfigOld.class);
    //private CasAuthenticationProvider casAuthenticationProvider;
    //private ServiceProperties serviceProperties;

    //@Autowired
//    public CASWebSecurityConfigOld(CasAuthenticationProvider casAuthenticationProvider,
//                                   ServiceProperties serviceProperties) {
///*        this.logoutFilter = logoutFilter;
//        this.singleSignOutFilter = singleSignOutFilter;*/
//        this.serviceProperties = serviceProperties;
//        //this.casAuthenticationProvider = casAuthenticationProvider;
//    }

    /*@Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/")
                .authenticated()
                .and().exceptionHandling()
                .authenticationEntryPoint(authenticationEntryPoint());
    }*/

//    @Override
//    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth.authenticationProvider(casAuthenticationProvider);
//    }
//
//    @Bean
//    @Override
//    protected AuthenticationManager authenticationManager() throws Exception {
//        return new ProviderManager(Collections.singletonList(casAuthenticationProvider));
//    }
//
//    @Bean
//    public CasAuthenticationEntryPoint authenticationEntryPoint() {
//        CasAuthenticationEntryPoint entryPoint = new CasAuthenticationEntryPoint();
//        entryPoint.setLoginUrl("https://localhost:8443/cas/login");
//        entryPoint.setServiceProperties(this.serviceProperties);
//        return entryPoint;
//    }


}
